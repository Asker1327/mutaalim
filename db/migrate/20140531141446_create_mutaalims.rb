class CreateMutaalims < ActiveRecord::Migration
  def change
    create_table :mutaalims do |t|
      t.string  :question
      t.text  	:answers
      t.timestamps
    end
  end
end
