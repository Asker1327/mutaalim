# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
test_file = File.open(Rails.public_path.to_s + "/mutaAlim.json","rb")
test_hash = JSON.parse(test_file.read)
test_file.close


test_hash.each_index do |i|  
	buf = ""
 	test_hash[i]["answers"].each { |j| buf + j + '&'}
	Mutaalim.create(question: test_hash[i]["question"], answers: buf)
end