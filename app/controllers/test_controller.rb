# coding: utf-8
class TestController < ApplicationController
  def index

  	@kol_testov = 13

  	#если только попали на страницу теста 
  	#подготавливаем все что необходимо
  	if (cookies[:testcookies] == false) or (cookies[:testcookies] == nil)
  		#считываем вопросы в хеш
		test_file = File.open('public/mutaAlim.json',"rb")
		params[:tests] = JSON.parse(test_file.read)
		test_file.close

		#массив для хранения случайно выбранных номеров тестов из общей базы
		cookies[:mas_test_number] = Array.new(@kol_testov)
		#массив для хранения ответов пользователя
		cookies[:mas_test_answer] = Array.new(@kol_testov) { |i| -1}

		# заносим случайные вопросы в массив mas_test_number
		cookies[:mas_test_number][0] = Random.rand(params[:tests] .length)
		cookies[:mas_test_number].each_index do |i| 
			begin
				flag = true
				buf = Random.rand(params[:tests] .length)
				0..i.times do |j|
					if cookies[:mas_test_number][j]==buf
						flag = false
					end
				end
				if flag
					cookies[:mas_test_number][i] = buf
				end
			end until flag==true
	    end
		cookies[:test_number] = 0;
		
		# заносим случайные варианты ответов в массив cookies[:mas_random_variants]
		cookies[:mas_random_variants] =  Array.new(@kol_testov)
		cookies[:mas_random_variants].each_index do |i| 
			cookies[:mas_random_variants][i] = Array.new(params[:tests] [cookies[:mas_test_number][i]]["answers"].size)
		end
		cookies[:mas_random_variants].each_index do |i|
			cookies[:mas_random_variants][i][0] = Random.rand(params[:tests] [cookies[:mas_test_number][i]]["answers"].size)
			cookies[:mas_random_variants][i].each_index do |j|
				begin
					flag = true
					buf = Random.rand(params[:tests] [cookies[:mas_test_number][i]]["answers"].size)
					0..j.times do |k|
						if cookies[:mas_random_variants][i][k]==buf
							flag = false
						end
					end
					if flag
						cookies[:mas_random_variants][i][j] = buf
					end
				end until flag==true
			end
		end
		
		cookies[:testcookies] == true
	end

	#занесение ответа в массив по нажатию на кнопку ответить 
  	if cookies[:answerId] !=nil
  		#сохраняем выбранный вариант ответа в массив cookies[:@mas_test_answer]
		cookies[:mas_test_answer][cookies[:test_number]] = cookies[:answerId].to_i
		#если возможно выдаем след вопрос
		if cookies[:test_number] != @kol_testov - 1
			cookies[:test_number] += 1
		end
	end
  	
  	#переход на любой вопрос по ссылке
	if cookies[:questionId] != nil
		cookies[:test_number] = cookies[:questionId].to_i - 1
	end

	#переход на следующий и предыдущий вопросы по кнопкам
	if cookies[:number] != nil
		if cookies[:number] == 'next'
			if cookies[:test_number] != @kol_testov - 1
					cookies[:test_number] = cookies[:test_number] + 1
			end
		end
		if cookies[:number] == 'prev'
		  	if cookies[:test_number] != 0
					cookies[:test_number] -= 1
			end
		end
	end

  end
end
